﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Timer : MonoBehaviour
{
    public static Timer instance;
    public bool StartTimer = false;
    public bool canPlay = true;
    public float timer = 0;
    private float cd = 180;
    
    public Text second;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (StartTimer == true)
        {
            if(second != null){
            second.gameObject.SetActive(true);
            second.text = ((int)(cd - timer)).ToString() +"秒后可以再次点击";
            }
            timer += Time.deltaTime;
            
        }else{
            if(second != null){
            second.gameObject.SetActive(false);
            }
        }
        if(timer >= cd){
            canPlay = true;
            StartTimer = false;
            timer = 0;
        }
    }
    // public void Delete(){
    //     Destroy(this.gameObject);
    // }
    // public void Test(){
    //     Debug.Log("play1");
    //     canPlay = false;
    //     StartCoroutine("WaitTime");
        
    // }
    // IEnumerator WaitTime(){
    //     yield return new WaitForSeconds(2f);
    //     if(buttonType == 1){
    //         Debug.Log(buttonType);
    //         rewardType = buttonType;
    //         Debug.Log(rewardType);
    //     }
        
    // }
}
