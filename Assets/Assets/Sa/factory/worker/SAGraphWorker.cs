﻿using UnityEngine;
using UnityEngine.UI;

using Sacu.Utils;

using Sacu.Collection;
using System.Collections.Generic;
using Google.Producer.Events;
using Datas;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Sacu.Factory.Worker
{
    public class SAGraphWorker : SABaseGraphWorker
    {
        

        override public void destroyDisplay()
        {
            base.destroyDisplay();
        }
        override protected void init()
        {
            //这里初始化Lua
            originName = patchName.Substring(patchName.IndexOf("/") + 1);// NoDotName.Replace(SAAppConfig.PrefabUIDir, "");
            luaName = displayName.Substring(displayName.LastIndexOf(".") + 1);
            
            setActive(false);//初始化之后
            HideVec3 = SAManager.Instance.HideVec3;
            base.init();

        }

        public void BindAtlas(string name)
        {
            //绑定图集，需要换成UGUI
            //UISprite[] sprites = SAGameObject.GetComponentsInChildren<UISprite>();
            //UISprite sprite;
            //for (int i = 0; i < sprites.Length; ++i)
            //{
            //    sprite = sprites[i];
            //    sprite.atlas = (UIAtlas)SACache.getObjectWithName(name);
            //}
        }
        


        public GameObject getGameObjectForName(string objectName)
        {
            return SATransform.Find(objectName).gameObject;
        }
        public GameObject getGameObjectChild(GameObject go,string objectName)
        {
            return go.transform.Find(objectName).gameObject;
        }
        
        public UnityEngine.Component getGameObjectByTypeName(string parentName, string typeName)
        {
            string str = parentName + " ====== getGameObjectByTypeName " + typeName;
            Debug.Log(str);
            return SATransform.Find(parentName).GetComponent(typeName);
        }
        public T getComponentForGameObjectName<T>(string objectName)
        {
            return getGameObjectForName(objectName).GetComponent<T>();
        }

        public GameObject InstantiateToTransform(GameObject original)
        {
            GameObject go = Object.Instantiate<GameObject>(original);
            go.transform.SetParent(SATransform);
            return go;
            //return Object.Instantiate(original);
        }
        public void removeListenerButtonClick(string buttonName)
        {
            removeListenerButtonClick(getGameObjectForName(buttonName).GetComponent<Button>());
        }
        public void removeListenerButtonClick(Button button)
        {
            SALang.removeListenerButtonClick(button);
            
        }
        public void setAtlasName()
        {

        }
    }
}