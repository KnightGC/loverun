﻿using Sacu.Collection;
using System.IO;
using Sacu.Utils;

namespace Sacu.Factory.Worker
{
    public class SADataWorker : SABaseDataWorker
    {
        protected string originName;
        /**
        * 构造函数
        * @param name 工人名称。
        * @param command HTTPInterface的调用命令。
        */
        public SADataWorker(string name, string command):base(name, command){
            //这里初始化Lua
            originName = name.Replace(".", "/");
        }
    }
}