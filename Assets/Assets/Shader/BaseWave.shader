﻿Shader "Custom/BaseWave"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _A ("A", float) = 1.0
        _Frenquncy ("Frenquncy", float) = 0.5
        _Speed ("Speed", float) = 0.5
    }
    /**
    顶点着色器
        1.计算顶点位置
        2.矩阵转换
    片段着色器
        1.纹理寻址
        2.灯光计算


    */
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            float _A;
            float _Frenquncy;
            float _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                /**
                _Time是CG api中代表时间周期的函数
                _Time.x = time / 20
                _Time.y = time
                _Time.z = time * 2
                _Time.w = time * 3
                */
                //_A 位移比例,峰值
                //time 时间周期
                //v.vertex.x * _Speed 位移距离
                float waver = _A * sin(_Time.y + v.vertex.x * _Speed);
                v.vertex.y = v.vertex.y + waver;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
            /**
            关于贴图的一个设置
            Warp Mode
                1.Repeat,重复模式
                2.Clamp,拉伸模式
                3.Mirror,镜像重复模式
                4.Mirror Once,镜像重复模式1次
                5.Per-axis,使用以上四种模式自定义横向和纵向
            */
            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                //fixed4 col = tex2D(_MainTex, i.uv);

                float2 tempUV = i.uv;
                tempUV.x -= _Time.y * _Speed / 10;
                fixed4 col = tex2D(_MainTex, tempUV);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
