﻿Shader "Custom/SetTexture"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
        _Color2 ("Color2", Color) = (1, 1, 1, 1)
        _MainTex1 ("Texture1", 2D) = "white" {}
        _MainTex2 ("Texture2", 2D) = "white" {}
        _MainTex3 ("Texture3", 2D) = "white" {}
    }
     /**
    灯光计算公式
    Ambient * Lighting Windows's Ambient Intensity setting + (Light Color * Diffuse + Light Color * Specular) + Emission

    Ambient     环境光
    Diffuse     漫反射
    Specular    镜面反射
    Emission    自发光
    Light Color 灯光颜色
    Lighting Windows's Ambient Intensity setting        好像是场景的一个设置环境光

    反射类（漫反射、镜面反射）受光源影响
    环境光受Lighting Windows's Ambient Intensity setting影响（需要打开颜色环境光，并且调整三原色不为0）
    自发光为自身光源颜色

    一、顶点着色器：
        1.计算顶点颜色
        2.顶点变换
        3.灯光作用

    二、片段着色器
        纹理贴图：
            1.等比例纹理和区域

            2.纹理大于贴图区域 或 纹理小于贴图区域,采用 等比例映射,UV纹理寻址设置如下（贴图的Filter Mode属性）
                2.1.point(no filter),就近取样,通过比例直接取对应比例点的像素点，其他多余点舍弃
                2.2.bilinear,就近4点平均值取样,好像是point比例点周围四个点的平均值,(上+下+左+右+中) / 5
                2.3.trilinear,就近8点平均值取样,好像是point比例点周围八个点的平均值,(上+下+左+右+中+左上+右上+右下+左下) / 9
        设置纹理:
            1.SetTexture[TextureName]{Texture Block}
                1.1.TextureName:纹理变量名
                1.2.Texture Block代码模块
            2.Texture Block关键字
                2.1.Previous:表示前面一个SetTexture出来以后的像素。
                2.2.Primary:表示当前顶点计算出来的颜色。
                2.3.Texture:表示等于SetTexture当前的纹理变量。
                2.4.Constant:表示一个固定的颜色。
    三、三大测试
    */
    SubShader{
        Pass
        {
            Color[_Color]

            SetTexture[_MainTex1]{
                combine Texture * Primary
			}

            //lerp融合
            // (1 - alpha)Texture + alpha * Previous
            SetTexture[_MainTex2]{
                constantColor[_Color2]
                combine Texture lerp(Primary) Previous
                //combine Constant * Texture
                //combine Primary * Texture
			}
        }
        Pass
        {
            SetTexture[_MainTex2]{
                constantColor[_Color2]
                combine Constant * Texture
                //combine Primary * Texture
			}
		}
    }
}
