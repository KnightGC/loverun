﻿Shader "Custom/Rotate"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Speed ("Speed", float) = -5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"


            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            float _Speed;
            v2f vert (appdata v)
            {
                v2f o;
                
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }


            /**
            矩阵旋转公式
            [x, y, z]
            [x, y, z]
            [x, y, z]

                    x = cos Angle, -sin Angle, 0
            Rz =    y = sin Angle, cos Angle, 0
                    z = 0, 0, 1
            
                    x = 1, 0, 0
            Rx =    y = 0, cos Angle, -sin Angle
                    z = 0, sin Angle, cos Angle

                    x = cos Angle, 0, sin Angle
            Ry =    y = 0, 1, 0
                    z = -sin Angle, 0, cos Angle

            




            */
            fixed4 frag (v2f i) : SV_Target
            {
                float timer = _Time.y * _Speed;
                //实现思路，先将中心移动到原点，旋转后再偏移回来
                float2 tempUV = i.uv - 0.5;
                if(length(tempUV) > 0.5){
                    return fixed4(0, 0, 0, 0);
				}
                float2 result;
                result.x = tempUV.x * cos(timer) - tempUV.y * sin(timer);
                result.y = tempUV.x * sin(timer) + tempUV.y * cos(timer);

                result += 0.5;
                //tempUV.y = tempUV.y * cos(_Time.y);

                // sample the texture
                fixed4 col = tex2D(_MainTex, result);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
