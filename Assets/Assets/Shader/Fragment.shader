﻿Shader "Custom/Fragment"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    /**
    灯光计算公式
    Ambient * Lighting Windows's Ambient Intensity setting + (Light Color * Diffuse + Light Color * Specular) + Emission

    Ambient     环境光
    Diffuse     漫反射
    Specular    镜面反射
    Emission    自发光
    Light Color 灯光颜色
    Lighting Windows's Ambient Intensity setting        好像是场景的一个设置环境光

    反射类（漫反射、镜面反射）受光源影响
    环境光受Lighting Windows's Ambient Intensity setting影响（需要打开颜色环境光，并且调整三原色不为0）
    自发光为自身光源颜色

    一、顶点着色器：
        1.计算顶点颜色
        2.顶点变换
        3.灯光作用

    二、片段着色器
        纹理贴图：
            1.等比例纹理和区域

            2.纹理大于贴图区域 或 纹理小于贴图区域,采用 等比例映射,UV纹理寻址设置如下（贴图的Filter Mode属性）
                2.1.point(no filter),就近取样,通过比例直接取对应比例点的像素点，其他多余点舍弃
                2.2.bilinear,就近4点平均值取样,好像是point比例点周围四个点的平均值,(上+下+左+右+中) / 5
                2.3.trilinear,就近8点平均值取样,好像是point比例点周围八个点的平均值,(上+下+左+右+中+左上+右上+右下+左下) / 9


    三、三大测试
    */
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                //UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                col.a = 1;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
