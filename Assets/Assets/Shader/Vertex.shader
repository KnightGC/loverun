﻿Shader "Custom/Vertex"
{
    Properties
    {
        _Color ("Color", Color) = (1, 1, 1, 1)
        _DiffuseColor ("DiffuseColor", Color) = (1, 1, 1, 1)
        _AmbientColor ("AmbientColor", Color) = (1, 1, 1, 1)
        _SpecularColor ("SpecularColor", Color) = (1, 1, 1, 1)
        _EmissionColor ("EmissionColor", Color) = (1, 1, 1, 1)
        
    }
    /**
    灯光计算公式
    Ambient * Lighting Windows's Ambient Intensity setting + (Light Color * Diffuse + Light Color * Specular) + Emission

    Ambient     环境光
    Diffuse     漫反射
    Specular    镜面反射
    Emission    自发光
    Light Color 灯光颜色
    Lighting Windows's Ambient Intensity setting        好像是场景的一个设置环境光

    反射类（漫反射、镜面反射）受光源影响
    环境光受Lighting Windows's Ambient Intensity setting影响（需要打开颜色环境光，并且调整三原色不为0）
    自发光为自身光源颜色

    一、顶点着色器：
        1.计算顶点颜色
        2.顶点变换
        3.灯光作用

    二、片段着色器


    三、三大测试
    */
    SubShader
    {
        Pass
        {
            Blend One One
            //Color (0, 1, 0, 1)
            Color [_Color]
            Material {
                Diffuse [_DiffuseColor]//漫反射
                Ambient [_AmbientColor]//环境光
                Specular [_SpecularColor]//镜面反射
                Emission [_EmissionColor]//自发光
            }
            Lighting On
            SeparateSpecular On
		}
    }
}
