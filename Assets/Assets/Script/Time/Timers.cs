﻿using Sacu.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class Timers : MonoBehaviour
{
    /*private static Timers _instance;

    public static Timers Instance
    {
        get
        {
            if (null == _instance)
            {
                _instance = new Timers();
            }
            return _instance;
        }
    }*/
    public string key;
    private float timerss;
    public bool a ;
    public bool b;

    protected long time;
    public GameObject go3;
    public GameObject go4;
    // Start is called before the first frame update
   /* public string GetKey(string s)
    {
        key = s;
        return key;
    }*/
    void Start()
    {
        key = Static.key;
        Application.runInBackground = false;
        a = Static.IsGet;

        if (PlayerPrefs.HasKey(key))
        {
            string s = PlayerPrefs.GetString(key);
            timerss = float.Parse(s);
        }
        else
        {
            timerss = 0f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        string[] Day = new string[] { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
        string week = Day[Convert.ToInt32(DateTime.Now.DayOfWeek.ToString("d"))].ToString();
        timerss += Time.deltaTime;

        if (a)
        {
            if (week == "星期六" || week == "星期日")
            {
                if (timerss > 10800f)
                {
                    SAManager.Instance._init = false;
                    GameObject go2 = Resources.Load<GameObject>("UI/UIRoot");
                    go3 = Instantiate(go2);
                    go3.transform.localPosition = Vector3.zero;
                    go3.transform.localRotation = Quaternion.identity;
                    go3.transform.localScale = Vector3.one;
                    go3.transform.SetAsLastSibling();
                    DontDestroyOnLoad(go3.gameObject);

                    GameObject go = Resources.Load<GameObject>("UI/AttestationUI/cn_PopUI");
                    GameObject go1 = Instantiate(go);
                    go1.transform.SetParent(go3.gameObject.transform);
                    go1.transform.localPosition = Vector3.zero;
                    go1.transform.localRotation = Quaternion.identity;
                    go1.transform.localScale = Vector3.one;
                    go1.transform.SetAsLastSibling();
                    //go1.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(but);
                    //Invoke("buts", 5);
                    PlayerPrefs.SetString(key, timerss.ToString());
                    a = false;
                    Destroy(this.gameObject);

                }
            }
            else if (timerss > 5400f)
            {
                SAManager.Instance._init = false;
                GameObject go2 = Resources.Load<GameObject>("UI/UIRoot");
                go4 = Instantiate(go2);
                go4.transform.localPosition = Vector3.zero;
                go4.transform.localRotation = Quaternion.identity;
                go4.transform.localScale = Vector3.one;
                go4.transform.SetAsLastSibling();
                DontDestroyOnLoad(go4.gameObject);
                GameObject go = Resources.Load<GameObject>("UI/AttestationUI/cn_PopUI");
                GameObject go1 = Instantiate(go);
                go1.transform.SetParent(go4.gameObject.transform);
                go1.transform.localPosition = Vector3.zero;
                go1.transform.localRotation = Quaternion.identity;
                go1.transform.localScale = Vector3.one;
                go1.transform.SetAsLastSibling();
                //go1.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(but);
                //Invoke("buts", 5);
                PlayerPrefs.SetString(key, timerss.ToString());
                a = false;
                Destroy(this.gameObject);

            }
        }
        
    }
    void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            time = DateTime.Now.Ticks;
        }
        else
        {
            time = (DateTime.Now.Ticks - time) / 10000000;
            SAUtils.Console("经过" + time + "秒");
            timerss = timerss + time;
                
        }
    }
    private void OnDestroy()
    {
        
       
    }
    public void buts()
    {
        a = false;
    }
    public void but()
    {
        
        Destroy(this.gameObject);
        SceneManager.LoadScene("00_logo");

    }
}
