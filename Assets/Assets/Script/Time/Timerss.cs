﻿using Sacu.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;
//检测时间段的代码
public class Timerss : MonoBehaviour
{
    void Start()
    {
       
    }

    void Update()
    {
        
        DateTime NowTime = DateTime.Now.ToLocalTime();
        string Hour = (NowTime.ToString("yyyyMMdd HH:mm:ss")).Substring(9, 2);
        string Fen = (NowTime.ToString("yyyyMMdd HH:mm:ss")).Substring(12, 2);
        int hour = Convert.ToInt32(Hour);
        int fen = Convert.ToInt32(Fen); 
        if (hour <= 7 || hour >= 22)
        {
            SAManager.Instance._init = false;
            GameObject go2 = Resources.Load<GameObject>("UI/UIRoot");
            GameObject go3 = Instantiate(go2);
            go3.transform.localPosition = Vector3.zero;
            go3.transform.localRotation = Quaternion.identity;
            go3.transform.localScale = Vector3.one;
            go3.transform.SetAsLastSibling();

            GameObject go = Resources.Load<GameObject>("UI/AttestationUI/cn_PopUI");
            GameObject go1 = Instantiate(go);
            go1.transform.SetParent(go3.gameObject.transform);
            go1.transform.localPosition = Vector3.zero;
            go1.transform.localRotation = Quaternion.identity;
            go1.transform.localScale = Vector3.one;
            go1.transform.SetAsLastSibling();
            go1.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(but);

        }
    }
    private void OnDestroy()
    {
    }
    public void but()
    {
        SceneManager.LoadScene("00_logo");
        Destroy(this.gameObject);
    }
    
}

