﻿using UnityEngine;
using Sacu.Factory;
using Sacu.Collection;
using Sacu.Utils;
using Graphs;
namespace Factory
{
    public class LoginUIFactory : SAFactory
    {
        public LoginUIFactory(string name) : base(name)
        {
        }

        override public void joinGraph(IOCGraphWorker workerData)
        {
            GameObject display;
            LoginUIGraphWorker graph;
            string DotName = workerData.name.Replace("/", ".");
            string NoDotName = workerData.name.Replace(".", "/");
            if (repeatObjects.ContainsKey(DotName))
            {
                display = repeatObjects[DotName];
            }
            else
            {
                display = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("UI/LoginUI/" + SAAppConfig.Language + "_LoginUI"));
                repeatObjects.Add(DotName, display);
            }

            //shell 1
            /*GameObject shell = new GameObject();
            shell.name = display.name + "[SHELL]";
            shell.layer = display.layer;*/
            //转换原始root  2
            /*GameObject original = new GameObject();
            original.layer = display.layer;
            original.name = display.name;
            original.transform.SetParent(shell.transform);
            Vector3 localPos;
            Transform child;
            while (display.transform.childCount > 0)
            {
                Debug.Log(display.transform.GetChild(0).name);
                child = display.transform.GetChild(0);
                localPos = child.transform.localPosition;
                child.SetParent(original.transform);
                child.GetComponent<RectTransform>().localPosition = localPos;
            }
            UnityEngine.Object.Destroy(display);
            display = original;*/

            //display.transform.SetParent(shell.transform);//shell 1

            //if (null != SAManager.Instance.UIRootCamera && shell.layer == SAManager.Instance.UGUILayer)//shell 1
            if (null != SAManager.Instance.UIRootCamera && display.layer == SAManager.Instance.UGUILayer)
            {
                //shell.transform.SetParent(SAManager.Instance.UIRootCamera.transform, false);//shell 1
                display.transform.SetParent(SAManager.Instance.UIRoot.transform, false);
                display.GetComponent<Transform>().localPosition = Vector3.zero;
                //display.GetComponent<Transform>().sizeDelta = new Vector2(Screen.width, Screen.height);
            }
            //笔记
            Transform _SATransform = display.transform;
            GameObject _SAGameObject = display.gameObject;
            //display = shell;//shell 1

            graph = display.AddComponent<LoginUIGraphWorker>();

            graph.setOrigin(_SAGameObject, _SATransform);
            graph.setActiveDispose(workerData.isActiveDispose);
            graph.setDefaultStart(workerData.defaultStart);

            graph._init(DotName, NoDotName);
            graphWorkers.Add(graph);
            if (_start)
            {
                registerGraphicsWorker(graph);
            }
        }
    }
}

