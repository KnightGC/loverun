﻿using UnityEngine;
using Sacu.Factory;
using Sacu.Collection;
using Sacu.Utils;
using Graphs;
namespace Factory
{
    public class DailyUIFactory : SAFactory
    {
        public DailyUIFactory(string name) : base(name)
        {
        }
        override public void startFactory(System.Object args)
        {
            this.args = args;
            if (!_init)
            {
                onInitFactory();
            }
            onStartFactory();
        }
        override protected void onStartFactory()
        {//执行工厂因子
            if(_start)
            {
               dispatchEvent(ActionCollection.DailyUI + ActionCollection.Open, ActionCollection.DailyMusic);
               Debug.Log("监听完成");
               //添加daily的监听
            } 
            else 
            {
                changeWorkerState(true);
            }
        }
        public override void disposeFactory()
        {
            dispatchEvent(ActionCollection.DailyQNS + ActionCollection.Close);
            dispatchEvent(ActionCollection.DailyMusic + ActionCollection.Close);
            dispatchEvent(ActionCollection.DailyPng + ActionCollection.Close);
        }
       
    }
}

