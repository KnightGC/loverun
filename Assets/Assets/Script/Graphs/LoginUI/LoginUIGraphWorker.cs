﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


using Datas;
using System;

//Graphs1  未启动
namespace Graphs
{
    public class LoginUIGraphWorker : SAGraphWorker
    {
        
        protected GameObject sureButton;
        protected GameObject sureButton_A;

        protected GameObject RegisterButton;

        protected GameObject userNameTxtGo;
        protected GameObject PassWordTxtGo;
        protected GameObject Loading;
        protected GameObject UIroot;
        protected GameObject go3;

        protected Text InfoLbl;

        public  string userNameTxt;
        public string passWordTxt1;
        public string passWordTxt2;
        public string bin;

        override protected void init()
        {
            
            base.init();
            bin = "sjdfhjd9347y34";
            Debug.Log(SystemInfo.deviceUniqueIdentifier);
            sureButton = GameObject.Find("UIRoot/cn_LoginUI(Clone)/SingleBtn");
            //RegisterButton = GameObject.Find("UIRoot/cn_LoginUI(Clone)/SingleBtn (1)");
            //sureButton_A = GameObject.Find("UIRoot/cn_LoginUI(Clone)/SingleBtn (1)_A");

            userNameTxtGo = GameObject.Find("UIRoot/cn_LoginUI(Clone)/UserNameTxt/Text");
            PassWordTxtGo = GameObject.Find("UIRoot/cn_LoginUI(Clone)/PassWordTxt/Text");
            
            InfoLbl = (Text)getGameObjectByTypeName("InfoLbl", "Text");
            
            
            UIroot = GameObject.Find("UIRoot");
            Loading = UIroot.transform.Find("cn_LoadingUI").gameObject;
            Loading.SetActive(false);
 
        }
        override protected void onRegister()
        {
            base.onRegister();
            SAManager.Instance.startFactory("ErrorCodeFactory");
            sureButton.GetComponent<Button>().onClick.AddListener(SingleSure);
            //RegisterButton.GetComponent<Button>().onClick.AddListener(StartRegister);
            //sureButton_A.GetComponent<Button>().onClick.AddListener(btn);
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SUserData, getUserInfoHandler);
            //addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SError, WeRegister);
            /*addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SRegister, WeLogins);*/

        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SUserData);
            
        }
        public void SingleSure()
        {
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            Debug.Log(sock.getConnection());
            if (sock.getConnection())
            {
                Sure();
            }
            else
            {
                connectHandle();
            }
        }
        public void Sure()
        {
            userNameTxt = userNameTxtGo.GetComponent<Text>().text;
            passWordTxt1 = PassWordTxtGo.GetComponent<Text>().text;
            
           
            if (userNameTxt.Length > 0 && passWordTxt1.Length > 0 )
            {
                
                CLogin.Builder cLogin = (CLogin.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CLogin);
                cLogin.SetUserName(userNameTxt);
                cLogin.SetPassWord(passWordTxt1);
                Datas.SocketDataWorker wokerRegister = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
                wokerRegister.sendMessage(ProtoTypeEnum.CLogin, cLogin.Build().ToByteArray());
                Debug.Log("发送了");
               /* Loading.SetActive(true);
                Loading.transform.SetAsLastSibling();*/
            }
            else
            {
                InfoLbl.text = "请输入账号和密码";
            }
        }
        public void connectHandle()
        {
            InfoLbl.text = "连接服务器......";
            addEventDispatcherWithHandle(CommandCollection.Sock + SAACollection.COMPLETE, ConnectComplete);
            addEventDispatcherWithHandle(CommandCollection.Sock + SAACollection.ERROR, ConnectError);

            SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
            Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
            sock.connect(version.ip, version.port);
        }
        public void ConnectComplete(FactoryEvent action)
        {
            InfoLbl.text = "服务器连接成功";
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
            Sure();
        }
        public void ConnectError(FactoryEvent action)
        {
            InfoLbl.text = "服务器连接成功";
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
        }

        private void StartRegister()
        {
            Loading.SetActive(false);
            factory.KillFactory();
            SAManager.Instance.startFactory("RegisterUIFactory");
        }
        public static long getSystemTime()
        {
            return System.DateTime.Now.Ticks;
        }
        private void getUserInfoHandler(FactoryEvent action)
        {
            long current=0;
            current = (getSystemTime() - current) / 10000;
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SUserData.Builder userData = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData, sdd.bytes);
            Static.key = userData.UserName;
            Static.IsGet = true;
            /*if (!PlayerPrefs.HasKey(userData.UserName))
            {
                PlayerPrefs.DeleteKey("timers");
            }*/
            

            //SAManager.Instance.startFactory("MainUIFactory");
            //启动心跳
            SAManager.Instance.startFactory("HeartFactory");
            DateTime NowTime = DateTime.Now.ToLocalTime();
            string year = (NowTime.ToString("yyyyMMdd HH:mm:ss")).Substring(0, 4);
            string Hour = (NowTime.ToString("yyyyMMdd HH:mm:ss")).Substring(9, 2);
            //如果没实名  跳转到实名界面
            if (userData.UserName == bin)
            {
                if (userData.DailyOnline == 0 && PlayerPrefs.HasKey(userData.UserName))
                {
                    PlayerPrefs.DeleteKey(userData.UserName);
                }
                Instantiate(Resources.Load("Time/Timerss"));
                Instantiate(Resources.Load("Time/Timers"));
                DontDestroyOnLoad(GameObject.Find("Timers(Clone)"));
                DontDestroyOnLoad(GameObject.Find("Timerss(Clone)"));

                factory.KillFactory();
                SceneManager.LoadScene("S3");//要切换到的场景名
            }
            /*else if ((userData.Name ==""||userData.Identity==""))
            {
                factory.KillFactory();
                SAManager.Instance.startFactory("AttestationUIFactory");
            }*/
            
            //判断是否大于18周岁  如果不大于开始用90分钟-userData.DailyOnline去做倒计时
            //如果点击的是游客则  开始倒计时（可以考虑考虑存本地）
            else
            {
                if (userData.DailyOnline == 0 && PlayerPrefs.HasKey(userData.UserName))
                {
                    PlayerPrefs.DeleteKey(userData.UserName);
                }
                string con = userData.Identity;
                string a = con.Substring(6, 4);
                int result = Convert.ToInt32(year) - Convert.ToInt32(a);
                int hour = Convert.ToInt32(Hour);
                //晚上十点到上午十点禁止登录游戏
                //十点需要校验如果有小于18周岁 强制离线
                //记录每日登录时间并累积   周六日不超过三小时   周一到周五不超过一个半小时
                if (result<=18)
                {
                    //禁止游戏的时间段
                    if (hour <= 7|| hour >= 22)
                    {
                        gameObject.SetActive(false);
                        GameObject go2 = Resources.Load<GameObject>("UI/UIRoot");
                        go3 = Instantiate(go2);
                        go3.transform.localPosition = Vector3.zero;
                        go3.transform.localRotation = Quaternion.identity;
                        go3.transform.localScale = Vector3.one;
                        go3.transform.SetAsLastSibling();

                        GameObject go = Resources.Load<GameObject>("UI/AttestationUI/cn_PopUI");
                        GameObject go1 = Instantiate(go);
                        go1.transform.SetParent(go3.gameObject.transform);
                        go1.transform.localPosition = Vector3.zero;
                        go1.transform.localRotation = Quaternion.identity;
                        go1.transform.localScale = Vector3.one;
                        go1.transform.SetAsLastSibling();
                        //写一个直接
                        go1.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(btns);
                    }
                    //不禁止游戏  但是会计时
                    else
                    {
                        gameObject.SetActive(false);
                        SAManager.Instance._init = false;
                        GameObject go2 = Resources.Load<GameObject>("UI/UIRoot");
                        GameObject go3_ = Instantiate(go2);
                        go3_.transform.localPosition = Vector3.zero;
                        go3_.transform.localRotation = Quaternion.identity;
                        go3_.transform.localScale = Vector3.one;
                        go3_.transform.SetAsLastSibling();

                        GameObject go = Resources.Load<GameObject>("UI/AttestationUI/cn_PopUI_weichengnian");
                        GameObject go1 = Instantiate(go);
                        go1.transform.SetParent(go3_.gameObject.transform);
                        go1.transform.localPosition = Vector3.zero;
                        go1.transform.localRotation = Quaternion.identity;
                        go1.transform.localScale = Vector3.one;
                        go1.transform.SetAsLastSibling();
                        go1.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(btn);
                    }
                }
                else
                {
                    SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
                    if (!sock.getConnection())
                    {
                        SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
                        SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
                        Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
                        sock.connect(version.ip, version.port);
                    }
                    SceneManager.LoadScene("S3");//要切换到的场景名
                    factory.KillFactory();
                }
            }
        }
        public void btn()
        {
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            if (!sock.getConnection())
            {
                SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
                SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
                Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
                sock.connect(version.ip, version.port);
            }
            //实时检测用户到达时间段后  直接弹出页面强制下线  新建一个检测时间的物体和代码  
            //用存的本地的时间  直接update里面监测是否到达时间  一旦达到  弹出页面强制下线 
            Instantiate(Resources.Load("Time/Timerss"));
            Instantiate(Resources.Load("Time/Timers"));
            DontDestroyOnLoad(GameObject.Find("Timers(Clone)"));
            DontDestroyOnLoad(GameObject.Find("Timerss(Clone)"));
            /*DontDestroyOnLoad(GameObject.Find("Reporter"));*/
            
            factory.KillFactory();
            SceneManager.LoadScene("S3");//要切换到的场景名
        }
        public void btns()
        {
            if (go3.gameObject == null)
            {
                return;
            }
            Destroy(go3.gameObject);
            gameObject.SetActive(true);
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            sock.close();
            if (!sock.getConnection())
            {
                SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
                SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
                Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
                sock.connect(version.ip, version.port);
            }
        }
        /// <summary>
        /// 点击游客按钮自动登录
        /// 分为两种情况  一种是已经点击过游客的  一种是还没点击过的  利用返回的错误码去监听
        /// </summary>
        /// //10cd170ed6d9a24715ff676074e3ee2303ca8e01
        private void WeLogin()
        {
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            if (!sock.getConnection())
            {
                SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
                SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
                Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
                sock.connect(version.ip, version.port);
            }
            /*                   Loading.SetActive(true);
                               Loading.transform.SetAsLastSibling();*/
            
                CLogin.Builder cLogin = (CLogin.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CLogin);
                cLogin.SetUserName(bin);
                cLogin.SetPassWord("123");
                sock.sendMessage(ProtoTypeEnum.CLogin, cLogin.Build().ToByteArray());
        }
        /// <summary>
        /// 收到SError  触发注册
        /// </summary>
        /// <param name="action"></param>
        private void WeRegister(FactoryEvent action)
        {
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SError.Builder errorData = (SError.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SError, sdd.bytes);
            ErrorCodeEnum value = (ErrorCodeEnum)Enum.ToObject(typeof(ErrorCodeEnum), errorData.Code);

            if (value == ErrorCodeEnum.AccountIsFoundError)
            {
                /* WeLogin();*/
                InfoLbl.text = "账号或密码错误";
            }
            else if(value == ErrorCodeEnum.AccountNotFoundError)
            {
                /*Datas.SocketDataWorker wokerRegister = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");

                bool a = true;
                SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
                if (!sock.getConnection())
                {
                    SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
                    SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
                    Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
                    sock.connect(version.ip, version.port);
                }
                CRegister.Builder cRegister = (CRegister.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CRegister);
                cRegister.SetUserName(bin);
                cRegister.SetPassWord("123");
                cRegister.SetNickName("love");
                cRegister.SetTourist(a);

                sock.sendMessage(ProtoTypeEnum.CRegister, cRegister.Build().ToByteArray());*/
                InfoLbl.text = "账号或密码错误";

            }


        }
        private void WeLogins(FactoryEvent action)
            
        {
            Debug.Log(1111111);
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            /*                   Loading.SetActive(true);
                               Loading.transform.SetAsLastSibling();*/
            if (!sock.getConnection())
            {
                SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
                SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
                Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
                sock.connect(version.ip, version.port);
            }
            CLogin.Builder cLogin = (CLogin.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CLogin);
            cLogin.SetUserName(bin);
            cLogin.SetPassWord("123");
            sock.sendMessage(ProtoTypeEnum.CLogin, cLogin.Build().ToByteArray());
        }

    }
}

