using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;


using Datas;
using System;
//Graphs1  未启动
namespace Graphs
{
    public class RegisterUIGraphWorker : SAGraphWorker
    {
        protected GameObject sureButton;
        protected GameObject userNameTxtGo;
        protected GameObject PassWordTxtGo;
        protected Text InfoLbl;
        protected GameObject Loading;
        protected GameObject UIroot;

        public string userNameTxt;
        public string passWordTxt1;
        public string passWordTxt2;
        override protected void init()
        {
            base.init();
            sureButton = GameObject.Find("UIRoot/cn_RegisterUI(Clone)/SingleBtn");
            userNameTxtGo = GameObject.Find("UIRoot/cn_RegisterUI(Clone)/UserNameTxt/Text");
            PassWordTxtGo = GameObject.Find("UIRoot/cn_RegisterUI(Clone)/PassWordTxt/Text");
            InfoLbl = (Text)getGameObjectByTypeName("InfoLbl", "Text");
            Button sureButton1 = sureButton.GetComponent<Button>();
            sureButton1.onClick.AddListener(SingleSure);
           /* UIroot = GameObject.Find("UIRoot");
            Loading = UIroot.transform.Find("cn_LoadingUI").gameObject;*/
        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SRegister, getRegisterHandler);
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SError, getRegisterHandlers);

        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SUserData);
        }
        public void SingleSure()
        {
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            if (sock.getConnection())
            {
                Sure();
            }
            else
            {
                connectHandle();
            }
        }
        public void Sure()
        {
            bool a = false;
            userNameTxt = userNameTxtGo.GetComponent<Text>().text;
            passWordTxt1 = PassWordTxtGo.GetComponent<Text>().text;
            if (userNameTxt != null&& passWordTxt1 != null&& userNameTxt.Length>0&& passWordTxt1.Length>0)
            {
                CRegister.Builder cRegister = (CRegister.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CRegister);
                cRegister.SetUserName(userNameTxt);
                cRegister.SetPassWord(passWordTxt1);
                cRegister.SetNickName("love");
                cRegister.SetTourist(a);

                Datas.SocketDataWorker wokerRegister = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
                wokerRegister.sendMessage(ProtoTypeEnum.CRegister, cRegister.Build().ToByteArray());
                Loading.SetActive(true);
                Loading.transform.SetAsLastSibling();
            }
            else
            {
                InfoLbl.text = "请输入账号，密码和昵称";
            }
        }
        public void connectHandle()
        {
            InfoLbl.text = "连接服务器......";
            addEventDispatcherWithHandle(CommandCollection.Sock + SAACollection.COMPLETE, ConnectComplete);
            addEventDispatcherWithHandle(CommandCollection.Sock + SAACollection.ERROR, ConnectError);

            SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
            Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
            sock.connect(version.ip, version.port);
        }
        public void ConnectComplete(FactoryEvent action)
        {
            Debug.Log("链接成功");
            InfoLbl.text = "服务器连接成功";
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
            Sure();
        }
        public void ConnectError(FactoryEvent action)
        {
            InfoLbl.text = "服务器连接失败";
            Debug.Log("链接失败");
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
        }
        private void getRegisterHandler(FactoryEvent action)
        {
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SRegister .Builder userData = (SRegister.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SRegister, sdd.bytes);

            Datas.SocketDataWorker wokerRegister = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            wokerRegister.close();


            Debug.Log("reg 1");
            try
            {
                SAManager.Instance.startFactory("LoginUIFactory");

            } catch(System.Exception e)
            {
                Debug.Log(e.ToString());
            }
            Debug.Log("reg 2");
            factory.KillFactory();
        }
        private void getRegisterHandlers(FactoryEvent action)
        {
            Debug.Log("!!!!!!!!!");
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SError.Builder errorData = (SError.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SError, sdd.bytes);
            ErrorCodeEnum value = (ErrorCodeEnum)Enum.ToObject(typeof(ErrorCodeEnum), errorData.Code);
            if (value == ErrorCodeEnum.AccountIsFoundError)
            {
                InfoLbl.text = "账号已存在，请重新注册";
            }
            Datas.SocketDataWorker wokerRegister = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            wokerRegister.close();
        }
        public override void destroyDisplay()
        {
            base.destroyDisplay();
            Destroy(gameObject);

        }
    }
}
