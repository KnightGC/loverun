﻿using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Datas;

namespace Graphs
{
    public class ErrorCodeGraphWorker : SAGraphWorker
    {
        override protected void init()
        {
            base.init();

        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SError, errorCodeHandler);

        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SUserData);
        }
        private void errorCodeHandler(FactoryEvent action)
        {
            SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
            SError.Builder error = (SError.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SError, sdd.bytes);
            SAUtils.Console("错误码：" + error.Code);
            SAUtils.Console("错误码：" + (ErrorCodeEnum)error.Code);
            
        }
    }
}
