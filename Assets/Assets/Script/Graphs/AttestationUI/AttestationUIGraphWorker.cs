using Sacu.Factory.Worker;
using Sacu.Utils;
using Google.Producer.Events;
using org.jiira.protobuf;
using Sacu.Collection;

using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

using Datas;
using System.Text.RegularExpressions;
using System;
//Graphs1  未启动
namespace Graphs
{
    public class AttestationUIGraphWorker : SAGraphWorker    
    {
        protected GameObject sureButton;
        //protected GameObject sureButton1;
        protected GameObject go3_;
        protected GameObject userNameTxtGo;
        protected GameObject PassWordTxtGo;
        protected Text InfoLbl;
        protected GameObject Loading;
        protected GameObject UIroot;

        public string userNameTxt;
        public string passWordTxt1;
        public string passWordTxt2;
        override protected void init()
        {
            base.init();
            sureButton = GameObject.Find("UIRoot/cn_AttestationUI(Clone)/SingleBtn_A");
            //sureButton1 = GameObject.Find("UIRoot/cn_AttestationUI(Clone)/SingleBtn (1)_A");

            userNameTxtGo = GameObject.Find("UIRoot/cn_AttestationUI(Clone)/UserNameTxt_A/Text");
            PassWordTxtGo = GameObject.Find("UIRoot/cn_AttestationUI(Clone)/PassWordTxt_A/Text");
            InfoLbl = (Text)getGameObjectByTypeName("InfoLbl_A", "Text");
            UIroot = GameObject.Find("UIRoot");
            Loading = UIroot.transform.Find("cn_LoadingUI").gameObject;
            Loading.SetActive(false);

        }
        override protected void onRegister()
        {
            base.onRegister();
            addEventDispatcherWithHandle(CommandCollection.Sock + ProtoTypeEnum.SIdentity, getSIdentityHandler);
            //sureButton1.GetComponent<Button>().onClick.AddListener(SingleSure1);
            sureButton.GetComponent<Button>().onClick.AddListener(SingleSure);

        }

        override protected void onRemove()
        {
            base.onRemove();
            removeEventDispatcher(CommandCollection.Sock + ProtoTypeEnum.SIdentity);
        }
        public void SingleSure()
        {
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            Debug.Log(sock.getConnection());
            if (sock.getConnection())
            {
                InfoLbl.text = "服务器连接成功";
                Sure();
            }
            else
            {
                connectHandle();
            }
        }
        //判断输入是否为中文
        public bool IsChinese(string strInput)
        {

            Regex reg = new Regex("^[\u4e00-\u9fa5]+$");
            if (reg.IsMatch(strInput))
            {

                return true;
            }
            else
            {
                InfoLbl.text = "未通过验证";
                return false;
            }
        }
        public void Sure()
        {
            userNameTxt = userNameTxtGo.GetComponent<Text>().text;
            passWordTxt1 = PassWordTxtGo.GetComponent<Text>().text;

            
            if (userNameTxt == null || passWordTxt1 == null)
            {
                InfoLbl.text = "请输入姓名和身份证";
            }
            else if (passWordTxt1.Length == 15 || passWordTxt1.Length == 18)
            {
                if (userNameTxt.Length > 0 && userNameTxt.Length <= 5)
                {
                    //将所输入的身份证号码进行拆分，拆分为17位，最后一位留着待用
                    int Num0 = int.Parse(passWordTxt1.Substring(0, 1)) * 7;
                    int Num1 = int.Parse(passWordTxt1.Substring(1, 1)) * 9;
                    int Num2 = int.Parse(passWordTxt1.Substring(2, 1)) * 10;
                    int Num3 = int.Parse(passWordTxt1.Substring(3, 1)) * 5;
                    int Num4 = int.Parse(passWordTxt1.Substring(4, 1)) * 8;
                    int Num5 = int.Parse(passWordTxt1.Substring(5, 1)) * 4;
                    int Num6 = int.Parse(passWordTxt1.Substring(6, 1)) * 2;
                    int Num7 = int.Parse(passWordTxt1.Substring(7, 1)) * 1;
                    int Num8 = int.Parse(passWordTxt1.Substring(8, 1)) * 6;
                    int Num9 = int.Parse(passWordTxt1.Substring(9, 1)) * 3;
                    int Num10 = int.Parse(passWordTxt1.Substring(10, 1)) * 7;
                    int Num11 = int.Parse(passWordTxt1.Substring(11, 1)) * 9;
                    int Num12 = int.Parse(passWordTxt1.Substring(12, 1)) * 10;
                    int Num13 = int.Parse(passWordTxt1.Substring(13, 1)) * 5;
                    int Num14 = int.Parse(passWordTxt1.Substring(14, 1)) * 8;
                    int Num15 = int.Parse(passWordTxt1.Substring(15, 1)) * 4;
                    int Num16 = int.Parse(passWordTxt1.Substring(16, 1)) * 2;
                    int allNum = Num0 + Num1 + Num2 + Num3 + Num4 + Num5 + Num6 + Num7 + Num8 + Num9 + Num10 + Num11 + Num12 + Num13 + Num14 + Num15 + Num16;
                    bool a = Judge(allNum, passWordTxt1.Substring(17, 1));
                    bool b = IsChinese(userNameTxt);
                    if (/*a &&*/ b)
                    {
                        CIdentity.Builder cIdentity = (CIdentity.Builder)CommandCollection.getDataModel(ProtoTypeEnum.CIdentity);
                        cIdentity.SetName(userNameTxt);
                        cIdentity.SetIdentity(passWordTxt1);
                        Datas.SocketDataWorker wokerRegister = (Datas.SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
                        wokerRegister.sendMessage(ProtoTypeEnum.CIdentity, cIdentity.Build().ToByteArray());
                    }
                }
            }
           
            else
            {
                InfoLbl.text = "身份验证不通过";

            }
        }
        public  bool Judge(int allNum, string LastNum)
        {

            int Remainder = allNum % 11;
            //如果最后一位数不是X的时候将最后一位数转换为int
            if (Remainder == 0)
            {
                if (int.Parse(LastNum) == 1)
                {
                    return true;
                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 1)
            {
                if (int.Parse(LastNum) == 0)
                {
                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 2)
            {
                if (LastNum != "x" && LastNum != "X")
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
                else
                {
                    return true;

                }
            }
            if (Remainder == 3)
            {
                if (int.Parse(LastNum) == 9)
                {
                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 4)
            {
                if (int.Parse(LastNum) == 8)
                {
                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 5)
            {
                if (int.Parse(LastNum) == 7)
                {
                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 6)
            {
                if (int.Parse(LastNum) == 6)
                {
                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 7)
            {
                if (int.Parse(LastNum) == 5)
                {

                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 8)
            {
                if (int.Parse(LastNum) == 4)
                {

                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 9)
            {
                if (int.Parse(LastNum) == 3)
                {
                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            if (Remainder == 10)
            {
                if (int.Parse(LastNum) == 2)
                {
                    return true;

                }
                else
                {
                    InfoLbl.text = "您的身份证号不合法";
                    return false;

                }
            }
            return false;
        }
        public void connectHandle()
        {
            InfoLbl.text = "连接服务器......";
            addEventDispatcherWithHandle(CommandCollection.Sock + SAACollection.COMPLETE, ConnectComplete);
            addEventDispatcherWithHandle(CommandCollection.Sock + SAACollection.ERROR, ConnectError);

            SAUtils.Console(CommandCollection.Sock + SAACollection.COMPLETE);
            SocketDataWorker sock = (SocketDataWorker)Sacu.Utils.IOCManager.Instance.getIOCDataWorker("Datas.SocketDataWorker");
            SAGameVersion version = Sacu.Utils.SAGameVersion.Instance;
            Sacu.Utils.SAUtils.Console(version.ip + ":" + version.port);
            sock.connect(version.ip, version.port);
        }
        public void ConnectComplete(FactoryEvent action)
        {
            InfoLbl.text = "服务器连接成功";
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
            Sure();
        }
        public void ConnectError(FactoryEvent action)
        {
            InfoLbl.text = "服务器连接失败";
            removeEventDispatcher(CommandCollection.Sock + SAACollection.COMPLETE);
            removeEventDispatcher(CommandCollection.Sock + SAACollection.ERROR);
        }
        private void getSIdentityHandler(FactoryEvent action)
        {
            SUserData.Builder userData = (SUserData.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SUserData);
            DateTime NowTime = DateTime.Now.ToLocalTime();
            string year = (NowTime.ToString("yyyyMMdd HH:mm:ss")).Substring(0, 4);
            string Hour = (NowTime.ToString("yyyyMMdd HH:mm:ss")).Substring(9, 2);
            
            string con = passWordTxt1;
            string a = con.Substring(6, 4);
            Debug.Log(a);
        

            int result = Convert.ToInt32(year) - Convert.ToInt32(a);
            int hour = Convert.ToInt32(Hour);
            Debug.Log(result);
            //晚上十点到上午十点禁止登录游戏
            //十点需要校验如果有小于18周岁 强制离线
            //记录每日登录时间并累积   周六日不超过三小时   周一到周五不超过一个半小时
            if (result <= 18)
            {
                //禁止游戏的时间段
                if (hour <= 8 || hour >= 22)
                {
                    GameObject ui = GameObject.Find("UIRoot/cn_AttestationUI(Clone)");
                    Destroy(ui.gameObject);
                    GameObject go2 = Resources.Load<GameObject>("UI/UIRoot");
                    GameObject go3 = Instantiate(go2);
                    go3.transform.localPosition = Vector3.zero;
                    go3.transform.localRotation = Quaternion.identity;
                    go3.transform.localScale = Vector3.one;
                    go3.transform.SetAsLastSibling();

                    GameObject go = Resources.Load<GameObject>("UI/AttestationUI/cn_PopUI");
                    GameObject go1 = Instantiate(go);
                    go1.transform.SetParent(go3.gameObject.transform);
                    go1.transform.localPosition = Vector3.zero;
                    go1.transform.localRotation = Quaternion.identity;
                    go1.transform.localScale = Vector3.one;
                    go1.transform.SetAsLastSibling();
                    //写一个直接
                    go1.transform.Find("Btn").GetComponent<Button>().onClick.AddListener(btns);
                    GameObject uis = GameObject.Find("UIRoot/cn_AttestationUI(Clone)");
                    uis.SetActive(false);
                }
                //不禁止游戏  但是会计时
                else
                {
                    SAManager.Instance._init = false;
                   
                    GameObject go2 = Resources.Load<GameObject>("UI/UIRoot");
                    go3_ = Instantiate(go2);
                    go3_.transform.localPosition = Vector3.zero;
                    go3_.transform.localRotation = Quaternion.identity;
                    go3_.transform.localScale = Vector3.one;
                    go3_.transform.SetAsLastSibling();

                    GameObject go_ = Resources.Load<GameObject>("UI/AttestationUI/cn_PopUI_weichengnian");
                    GameObject go1_ = Instantiate(go_);
                    go1_.transform.SetParent(go3_.gameObject.transform);
                    go1_.transform.localPosition = Vector3.zero;
                    go1_.transform.localRotation = Quaternion.identity;
                    go1_.transform.localScale = Vector3.one;
                    go1_.transform.SetAsLastSibling();
                    Debug.Log(1);
                    Button btn = go1_.transform.Find("Btn").GetComponent<Button>();
                    btn.onClick.AddListener(btnss);
                    Debug.Log(go1_.transform.Find("Btn"));
                    GameObject ui = GameObject.Find("UIRoot/cn_AttestationUI(Clone)");
                    ui.SetActive(false);
                }
            }
            else
            {
                Loading.SetActive(true);
                Loading.transform.SetAsLastSibling();
                SASocketDataDAO sdd = (SASocketDataDAO)action.Body;
                SIdentity.Builder levleData = (SIdentity.Builder)CommandCollection.getDataModel(ProtoTypeEnum.SIdentity, sdd.bytes);
                factory.KillFactory();
                SceneManager.LoadScene("S3");//要切换到的场景名
            }
           
        }
        protected void btnss()
        {
            Debug.Log("???");
            //实时检测用户到达时间段后  直接弹出页面强制下线  新建一个检测时间的物体和代码  
            //用存的本地的时间  直接update里面监测是否到达时间  一旦达到  弹出页面强制下线 
            Instantiate(Resources.Load("Time/Timerss"));
            Instantiate(Resources.Load("Time/Timers"));
            DontDestroyOnLoad(GameObject.Find("Timers(Clone)"));
            DontDestroyOnLoad(GameObject.Find("Timerss(Clone)"));
            factory.KillFactory();
            SceneManager.LoadScene("S3");//要切换到的场景名
        }
        public void btns()
        {
            Destroy(go3_.gameObject);
        }
        /* private void SingleSure1()
         {
             Loading.SetActive(true);
             Loading.transform.SetAsLastSibling();
             Instantiate(Resources.Load("Time/Timer"));
             DontDestroyOnLoad(GameObject.Find("Timer(Clone)"));
             factory.KillFactory();
             SceneManager.LoadScene("S3");//要切换到的场景名

         }*/
    }
}
