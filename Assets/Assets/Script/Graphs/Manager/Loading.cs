﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Loading : MonoBehaviour
{
    AsyncOperation asyncOperation; //声明一个异步变量
    public GameObject progress; //声明进度条对象
    public GameObject Text; //声明进度条上的显示文本

    //对以上变量进行初始化

    void Start()
    {
        progress.SetActive(true);
        progress = GameObject.FindWithTag("Fader");
        Text = GameObject.FindWithTag("Text");
        Debug.Log(Text);
        Text.GetComponent<Text>().text = "0";
        progress.GetComponent<Image>().fillAmount = 0f;
        StartCoroutine(loadScene()); //开启异步任务，进入loadScene方法
    }
    void Update()
    {
       
        Text.GetComponent<Text>().text = (float)asyncOperation.progress * 100 + 10 + "%"; //文本更新异步进度
        progress.GetComponent<Image>().fillAmount = (float)asyncOperation.progress + .1f;//进度条更新异步进度
    }
    IEnumerator loadScene()
    {
        yield return asyncOperation =SceneManager.LoadSceneAsync("01_menu0319");//读取完毕自动进入下一个场景
    }

}
