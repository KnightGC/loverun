﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreManager : MonoBehaviour
{
    public GameObject UiRoot;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //点击商城按钮事件
    public void StoreBtn()
    {
        if (GameObject.Find("UIRoot/Store(Clone)") !=null)
        {
            return;
        }
        GameObject go = Resources.Load<GameObject>("UI/AttestationUI/Store");
        GameObject go1 = Instantiate(go);
        go1.transform.SetParent(UiRoot.gameObject.transform);
        go1.transform.localPosition = Vector3.zero;
        go1.transform.localRotation = Quaternion.identity;
        go1.transform.localScale = Vector3.one;
        go1.transform.SetAsLastSibling();
    }
}
