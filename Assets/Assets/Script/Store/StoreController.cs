﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StoreController : MonoBehaviour
{
    public GameObject UIRoot;
    // Start is called before the first frame update
    void Start()
    {
        UIRoot = GameObject.Find("UIRoot").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void GoPop()
    {
        GameObject go = Resources.Load<GameObject>("UI/AttestationUI/Pop_Store");
        GameObject go1 = Instantiate(go);
        go1.transform.SetParent(UIRoot.gameObject.transform);
        go1.transform.localPosition = Vector3.zero;
        go1.transform.localRotation = Quaternion.identity;
        go1.transform.localScale = Vector3.one;
        go1.transform.SetAsLastSibling();
    }
    public void ClosePop()
    {
        Destroy(this.gameObject);
    }

}
