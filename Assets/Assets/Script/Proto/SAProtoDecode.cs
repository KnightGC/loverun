using System.Collections.Generic;
namespace org.jiira.protobuf {
	public class ConvertModel{
		private int x, y, len;
		private string iostring;
		private static ConvertModel instance;
		public static ConvertModel getInstance() {
			if (null == instance) {
				instance = new ConvertModel();
			}
			return instance;
		}
		public void setting(string iostring) {
			setting(iostring, 0, 0);
		}
		public void setting(string iostring, int x, int y) {
			len = iostring.Length;
			this.iostring = iostring;
			this.x = x;
			this.y = y;
		}
		private string read() {
			int offset = SAProtoDecode.decodeAssign.Length;
			x = iostring.IndexOf(SAProtoDecode.decodeAssign, x) + offset;
			y = iostring.IndexOf(SAProtoDecode.decodeSplit, x);
			if (y == -1) {
				y = iostring.IndexOf(SAProtoDecode.decodeEnd, x);//结尾保护
				offset = SAProtoDecode.decodeEnd.Length;
			}
			string value = iostring.Substring(x, y - x);
			x = y + offset;
			return value;
		}
		public int readInt() {
			return int.Parse(read());
		}
		public bool readBoolean() {
			return read().Equals(SAProtoDecode.isBooleanStr);
		}
		public float readFloat()
		{
			return float.Parse(read());
		}
		public string readString()
		{
			return read();
		}
		public string[] readArray()
		{
			x = iostring.IndexOf(SAProtoDecode.decodeArrayAssign, x) + SAProtoDecode.decodeArrayAssign.Length;
			y = iostring.IndexOf(SAProtoDecode.decodeArrayEnd, x);
			string value = iostring.Substring(x, y - x);
			x = y + SAProtoDecode.decodeArrayEnd.Length;
			return System.Text.RegularExpressions.Regex.Split(value,SAProtoDecode.splitStr);
		}
		public bool limit()
		{
			return x >= len;
		}
		public void flip()
		{
			x = 0;
			y = 0;
		}
	}
	public class SAProtoDecode
	{
		public const string SAString = "string";
		public const string SAInt = "int";
		public const string SAFloat = "float";
		public const string SABoolean = "boolean";
		public const string SAArray = "array";
		public const string isBooleanStr = "1";
		public const string splitStr = "\\*";
		public const string decodeAssign = "[:]";
		public const string decodeSplit = "[,]";
		public const string decodeEnd = "[}]";
		public const string decodeArrayAssign = "[:][{]";
		public const string decodeArrayEnd = "[}][,]";
		public const string decodeStrEnd = "\"";
		public const string classNameEnd = "[}][]]\"";
		public SAProtoDecode() {}
		/**
		 * 解析一套数据表
		 * @param iostring
		 */
		public static void parsing(string iostring)
		{
			int x = 0, y = 0, len = iostring.Length;
			string typeName;
			string endName;
			string fragmentStr;
			while (x < len) {
				x = iostring.IndexOf(decodeStrEnd, x) + 1;//查询类型名称首部
				if (x == -1)
					break;//解析完毕
				y = iostring.IndexOf(decodeStrEnd, x);//查询类型名称尾部
				typeName = iostring.Substring(x, y - x);//获得类型
				endName = classNameEnd + typeName + decodeStrEnd;//获得类型结尾(如果解析内容中包含了自身类型，无法解析)
				x = y + decodeAssign.Length + 1;//查询内容首部(跳过冒号)
				y = iostring.IndexOf(endName, x);//查询内容尾部
				fragmentStr = iostring.Substring(x, y - x);//获得内容
				x = y + endName.Length;
				setIOString(typeName, fragmentStr);
			}
		}
		public static void setIOString(string type, string iostring) {
			switch (type) {
				case STCharacterType: {
					STCharacter.parsing(iostring);
					break;
				}
				case STEnemyType: {
					STEnemy.parsing(iostring);
					break;
				}
				case STSkillType: {
					STSkill.parsing(iostring);
					break;
				}
				case STFeelItemType: {
					STFeelItem.parsing(iostring);
					break;
				}
				case STFeelItemNumType: {
					STFeelItemNum.parsing(iostring);
					break;
				}
				case STBaseRewardType: {
					STBaseReward.parsing(iostring);
					break;
				}
				case STCommonType: {
					STCommon.parsing(iostring);
					break;
				}
				case STInstagramType: {
					STInstagram.parsing(iostring);
					break;
				}
				case STLevelsType: {
					STLevels.parsing(iostring);
					break;
				}
				case STCharacterLevelType: {
					STCharacterLevel.parsing(iostring);
					break;
				}
				case STBgmType: {
					STBgm.parsing(iostring);
					break;
				}
				case STLevelBgmType: {
					STLevelBgm.parsing(iostring);
					break;
				}
				case STIllustrationType: {
					STIllustration.parsing(iostring);
					break;
				}
			}
		}
		public const string STCharacterType = "sTCharacter";
		public const string STEnemyType = "sTEnemy";
		public const string STSkillType = "sTSkill";
		public const string STFeelItemType = "sTFeelItem";
		public const string STFeelItemNumType = "sTFeelItemNum";
		public const string STBaseRewardType = "sTBaseReward";
		public const string STCommonType = "sTCommon";
		public const string STInstagramType = "sTInstagram";
		public const string STLevelsType = "sTLevels";
		public const string STCharacterLevelType = "sTCharacterLevel";
		public const string STBgmType = "sTBgm";
		public const string STLevelBgmType = "sTLevelBgm";
		public const string STIllustrationType = "sTIllustration";
	}
	public class STCharacter {
		public STCharacter() { }
		private static List<STCharacter> list;
		private static Dictionary<int, STCharacter> map;
		public static List<STCharacter> getList() {
			if (null == list) {
				list = new List<STCharacter>();
			}
			return list;
		}
		public static Dictionary<int, STCharacter> getMap() {
			if (null == map) {
				map = new Dictionary<int, STCharacter>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STCharacter> list = getList();
			Dictionary<int, STCharacter> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STCharacter sTCharacter;
			while(!buf.limit()) {
				sTCharacter= new STCharacter();
				sTCharacter.id = buf.readInt();
				sTCharacter.name = buf.readString();
				sTCharacter.life = buf.readInt();
				sTCharacter.robot = buf.readInt();
				sTCharacter.skill1 = buf.readInt();
				sTCharacter.skill2 = buf.readInt();
				sTCharacter.skill3 = buf.readInt();
				sTCharacter.hit = buf.readInt();
				sTCharacter.feelItem = buf.readInt();
				sTCharacter.levelReward = buf.readArray();
				sTCharacter.shootDownReward = buf.readArray();
				sTCharacter.intro = buf.readString();
				list.Add(sTCharacter);
				map.Add(sTCharacter.Id, sTCharacter);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
		private int life;
		public int Life {get{return life;}}
		private int robot;
		public int Robot {get{return robot;}}
		private int skill1;
		public int Skill1 {get{return skill1;}}
		private int skill2;
		public int Skill2 {get{return skill2;}}
		private int skill3;
		public int Skill3 {get{return skill3;}}
		private int hit;
		public int Hit {get{return hit;}}
		private int feelItem;
		public int FeelItem {get{return feelItem;}}
		private string[] levelReward;
		public string[] LevelReward {get{return levelReward;}}
		private string[] shootDownReward;
		public string[] ShootDownReward {get{return shootDownReward;}}
		private string intro;
		public string Intro {get{return intro;}}
	}
	public class STEnemy {
		public STEnemy() { }
		private static List<STEnemy> list;
		private static Dictionary<int, STEnemy> map;
		public static List<STEnemy> getList() {
			if (null == list) {
				list = new List<STEnemy>();
			}
			return list;
		}
		public static Dictionary<int, STEnemy> getMap() {
			if (null == map) {
				map = new Dictionary<int, STEnemy>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STEnemy> list = getList();
			Dictionary<int, STEnemy> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STEnemy sTEnemy;
			while(!buf.limit()) {
				sTEnemy= new STEnemy();
				sTEnemy.id = buf.readInt();
				sTEnemy.name = buf.readString();
				sTEnemy.life = buf.readInt();
				sTEnemy.damage = buf.readInt();
				sTEnemy.speed = buf.readFloat();
				sTEnemy.score = buf.readInt();
				sTEnemy.crashed = buf.readInt();
				sTEnemy.intro = buf.readString();
				list.Add(sTEnemy);
				map.Add(sTEnemy.Id, sTEnemy);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
		private int life;
		public int Life {get{return life;}}
		private int damage;
		public int Damage {get{return damage;}}
		private float speed;
		public float Speed {get{return speed;}}
		private int score;
		public int Score {get{return score;}}
		private int crashed;
		public int Crashed {get{return crashed;}}
		private string intro;
		public string Intro {get{return intro;}}
	}
	public class STSkill {
		public STSkill() { }
		private static List<STSkill> list;
		private static Dictionary<int, STSkill> map;
		public static List<STSkill> getList() {
			if (null == list) {
				list = new List<STSkill>();
			}
			return list;
		}
		public static Dictionary<int, STSkill> getMap() {
			if (null == map) {
				map = new Dictionary<int, STSkill>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STSkill> list = getList();
			Dictionary<int, STSkill> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STSkill sTSkill;
			while(!buf.limit()) {
				sTSkill= new STSkill();
				sTSkill.id = buf.readInt();
				sTSkill.name = buf.readString();
				sTSkill.type = buf.readInt();
				sTSkill.behavior = buf.readInt();
				sTSkill.value = buf.readInt();
				sTSkill.time = buf.readInt();
				sTSkill.intro = buf.readString();
				list.Add(sTSkill);
				map.Add(sTSkill.Id, sTSkill);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
		private int type;
		public int Type {get{return type;}}
		private int behavior;
		public int Behavior {get{return behavior;}}
		private int value;
		public int Value {get{return value;}}
		private int time;
		public int Time {get{return time;}}
		private string intro;
		public string Intro {get{return intro;}}
	}
	public class STFeelItem {
		public STFeelItem() { }
		private static List<STFeelItem> list;
		private static Dictionary<int, STFeelItem> map;
		public static List<STFeelItem> getList() {
			if (null == list) {
				list = new List<STFeelItem>();
			}
			return list;
		}
		public static Dictionary<int, STFeelItem> getMap() {
			if (null == map) {
				map = new Dictionary<int, STFeelItem>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STFeelItem> list = getList();
			Dictionary<int, STFeelItem> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STFeelItem sTFeelItem;
			while(!buf.limit()) {
				sTFeelItem= new STFeelItem();
				sTFeelItem.id = buf.readInt();
				sTFeelItem.name = buf.readString();
				sTFeelItem.value = buf.readInt();
				list.Add(sTFeelItem);
				map.Add(sTFeelItem.Id, sTFeelItem);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
		private int value;
		public int Value {get{return value;}}
	}
	public class STFeelItemNum {
		public STFeelItemNum() { }
		private static List<STFeelItemNum> list;
		private static Dictionary<int, STFeelItemNum> map;
		public static List<STFeelItemNum> getList() {
			if (null == list) {
				list = new List<STFeelItemNum>();
			}
			return list;
		}
		public static Dictionary<int, STFeelItemNum> getMap() {
			if (null == map) {
				map = new Dictionary<int, STFeelItemNum>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STFeelItemNum> list = getList();
			Dictionary<int, STFeelItemNum> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STFeelItemNum sTFeelItemNum;
			while(!buf.limit()) {
				sTFeelItemNum= new STFeelItemNum();
				sTFeelItemNum.id = buf.readInt();
				sTFeelItemNum.amount = buf.readInt();
				sTFeelItemNum.value = buf.readInt();
				list.Add(sTFeelItemNum);
				map.Add(sTFeelItemNum.Id, sTFeelItemNum);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private int amount;
		public int Amount {get{return amount;}}
		private int value;
		public int Value {get{return value;}}
	}
	public class STBaseReward {
		public STBaseReward() { }
		private static List<STBaseReward> list;
		private static Dictionary<int, STBaseReward> map;
		public static List<STBaseReward> getList() {
			if (null == list) {
				list = new List<STBaseReward>();
			}
			return list;
		}
		public static Dictionary<int, STBaseReward> getMap() {
			if (null == map) {
				map = new Dictionary<int, STBaseReward>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STBaseReward> list = getList();
			Dictionary<int, STBaseReward> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STBaseReward sTBaseReward;
			while(!buf.limit()) {
				sTBaseReward= new STBaseReward();
				sTBaseReward.id = buf.readInt();
				sTBaseReward.name = buf.readString();
				sTBaseReward.times = buf.readInt();
				sTBaseReward.value = buf.readInt();
				list.Add(sTBaseReward);
				map.Add(sTBaseReward.Id, sTBaseReward);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
		private int times;
		public int Times {get{return times;}}
		private int value;
		public int Value {get{return value;}}
	}
	public class STCommon {
		public STCommon() { }
		private static List<STCommon> list;
		private static Dictionary<int, STCommon> map;
		public static List<STCommon> getList() {
			if (null == list) {
				list = new List<STCommon>();
			}
			return list;
		}
		public static Dictionary<int, STCommon> getMap() {
			if (null == map) {
				map = new Dictionary<int, STCommon>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STCommon> list = getList();
			Dictionary<int, STCommon> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STCommon sTCommon;
			while(!buf.limit()) {
				sTCommon= new STCommon();
				sTCommon.id = buf.readInt();
				sTCommon.value = buf.readInt();
				sTCommon.intro = buf.readString();
				list.Add(sTCommon);
				map.Add(sTCommon.Id, sTCommon);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private int value;
		public int Value {get{return value;}}
		private string intro;
		public string Intro {get{return intro;}}
	}
	public class STInstagram {
		public STInstagram() { }
		private static List<STInstagram> list;
		private static Dictionary<int, STInstagram> map;
		public static List<STInstagram> getList() {
			if (null == list) {
				list = new List<STInstagram>();
			}
			return list;
		}
		public static Dictionary<int, STInstagram> getMap() {
			if (null == map) {
				map = new Dictionary<int, STInstagram>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STInstagram> list = getList();
			Dictionary<int, STInstagram> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STInstagram sTInstagram;
			while(!buf.limit()) {
				sTInstagram= new STInstagram();
				sTInstagram.id = buf.readInt();
				sTInstagram.missionName = buf.readString();
				sTInstagram.missionContent = buf.readArray();
				sTInstagram.prizeName1 = buf.readString();
				sTInstagram.prize1 = buf.readArray();
				sTInstagram.prizeName2 = buf.readString();
				sTInstagram.prize2 = buf.readArray();
				sTInstagram.condition = buf.readInt();
				sTInstagram.value1 = buf.readInt();
				sTInstagram.value2 = buf.readInt();
				list.Add(sTInstagram);
				map.Add(sTInstagram.Id, sTInstagram);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string missionName;
		public string MissionName {get{return missionName;}}
		private string[] missionContent;
		public string[] MissionContent {get{return missionContent;}}
		private string prizeName1;
		public string PrizeName1 {get{return prizeName1;}}
		private string[] prize1;
		public string[] Prize1 {get{return prize1;}}
		private string prizeName2;
		public string PrizeName2 {get{return prizeName2;}}
		private string[] prize2;
		public string[] Prize2 {get{return prize2;}}
		private int condition;
		public int Condition {get{return condition;}}
		private int value1;
		public int Value1 {get{return value1;}}
		private int value2;
		public int Value2 {get{return value2;}}
	}
	public class STLevels {
		public STLevels() { }
		private static List<STLevels> list;
		private static Dictionary<int, STLevels> map;
		public static List<STLevels> getList() {
			if (null == list) {
				list = new List<STLevels>();
			}
			return list;
		}
		public static Dictionary<int, STLevels> getMap() {
			if (null == map) {
				map = new Dictionary<int, STLevels>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STLevels> list = getList();
			Dictionary<int, STLevels> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STLevels sTLevels;
			while(!buf.limit()) {
				sTLevels= new STLevels();
				sTLevels.id = buf.readInt();
				sTLevels.name = buf.readString();
				sTLevels.preId = buf.readInt();
				sTLevels.bgm = buf.readInt();
				sTLevels.type = buf.readInt();
				sTLevels.sOF = buf.readArray();
				list.Add(sTLevels);
				map.Add(sTLevels.Id, sTLevels);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
		private int preId;
		public int PreId {get{return preId;}}
		private int bgm;
		public int Bgm {get{return bgm;}}
		private int type;
		public int Type {get{return type;}}
		private string[] sOF;
		public string[] SOF {get{return sOF;}}
	}
	public class STCharacterLevel {
		public STCharacterLevel() { }
		private static List<STCharacterLevel> list;
		private static Dictionary<int, STCharacterLevel> map;
		public static List<STCharacterLevel> getList() {
			if (null == list) {
				list = new List<STCharacterLevel>();
			}
			return list;
		}
		public static Dictionary<int, STCharacterLevel> getMap() {
			if (null == map) {
				map = new Dictionary<int, STCharacterLevel>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STCharacterLevel> list = getList();
			Dictionary<int, STCharacterLevel> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STCharacterLevel sTCharacterLevel;
			while(!buf.limit()) {
				sTCharacterLevel= new STCharacterLevel();
				sTCharacterLevel.id = buf.readInt();
				sTCharacterLevel.feel = buf.readInt();
				sTCharacterLevel.behavior = buf.readInt();
				sTCharacterLevel.value = buf.readInt();
				sTCharacterLevel.intro = buf.readString();
				list.Add(sTCharacterLevel);
				map.Add(sTCharacterLevel.Id, sTCharacterLevel);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private int feel;
		public int Feel {get{return feel;}}
		private int behavior;
		public int Behavior {get{return behavior;}}
		private int value;
		public int Value {get{return value;}}
		private string intro;
		public string Intro {get{return intro;}}
	}
	public class STBgm {
		public STBgm() { }
		private static List<STBgm> list;
		private static Dictionary<int, STBgm> map;
		public static List<STBgm> getList() {
			if (null == list) {
				list = new List<STBgm>();
			}
			return list;
		}
		public static Dictionary<int, STBgm> getMap() {
			if (null == map) {
				map = new Dictionary<int, STBgm>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STBgm> list = getList();
			Dictionary<int, STBgm> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STBgm sTBgm;
			while(!buf.limit()) {
				sTBgm= new STBgm();
				sTBgm.id = buf.readInt();
				sTBgm.name = buf.readString();
				list.Add(sTBgm);
				map.Add(sTBgm.Id, sTBgm);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
	}
	public class STLevelBgm {
		public STLevelBgm() { }
		private static List<STLevelBgm> list;
		private static Dictionary<int, STLevelBgm> map;
		public static List<STLevelBgm> getList() {
			if (null == list) {
				list = new List<STLevelBgm>();
			}
			return list;
		}
		public static Dictionary<int, STLevelBgm> getMap() {
			if (null == map) {
				map = new Dictionary<int, STLevelBgm>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STLevelBgm> list = getList();
			Dictionary<int, STLevelBgm> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STLevelBgm sTLevelBgm;
			while(!buf.limit()) {
				sTLevelBgm= new STLevelBgm();
				sTLevelBgm.id = buf.readInt();
				sTLevelBgm.name = buf.readString();
				list.Add(sTLevelBgm);
				map.Add(sTLevelBgm.Id, sTLevelBgm);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
	}
	public class STIllustration {
		public STIllustration() { }
		private static List<STIllustration> list;
		private static Dictionary<int, STIllustration> map;
		public static List<STIllustration> getList() {
			if (null == list) {
				list = new List<STIllustration>();
			}
			return list;
		}
		public static Dictionary<int, STIllustration> getMap() {
			if (null == map) {
				map = new Dictionary<int, STIllustration>();
			}
			return map;
		}
		public static void parsing(string iostring) {
			List<STIllustration> list = getList();
			Dictionary<int, STIllustration> map = getMap();
			list.Clear();
			map.Clear();
			ConvertModel buf = ConvertModel.getInstance();
			buf.setting(iostring);
			STIllustration sTIllustration;
			while(!buf.limit()) {
				sTIllustration= new STIllustration();
				sTIllustration.id = buf.readInt();
				sTIllustration.name = buf.readString();
				list.Add(sTIllustration);
				map.Add(sTIllustration.Id, sTIllustration);
			}
		}
		private int id;
		public int Id {get{return id;}}
		private string name;
		public string Name {get{return name;}}
	}
}