using org.jiira.protobuf;
using System.Collections.Generic;
namespace Sacu.Utils{
	public enum ProtoTypeEnum {
		CLogin, //
		CRegister, //
		SRegister, //
		CRegularization, //
		SRegularization, //
		SUserData, //
		CReNickName, //
		SReNickName, //
		CIdentity, //
		SIdentity, //
		CGetCharacterList, //
		SGetCharacterList, //
		CFeelReward, //
		SFeelReward, //
		CItems, //
		SItems, //
		CUseItem, //
		CBaseReward, //
		SBaseReward, //
		CShowAdv, //
		SSingleUpdate, //
		CLevels, //
		SLevels, //
		CInses, //
		SInses, //
		CInsComplete, //
		CInsRecord, //
		SInsRecord, //
		CInsRecords, //
		SInsRecords, //
		CMusic, //
		SMusic, //
		CPhotos, //
		SPhotos, //
		CGetPhoto, //
		CRank, //
		SRank, //
		SError, //
		CHeart, //
	}
	public enum SkillTypeEnum {
		Trigger, //主动技能
		Passive, //被动技能
	}
	public enum SkillBehaviorEnum {
		Unlock, //解锁
		Damage, //造成伤害
		Multiple, //增加上下分身
		RepairLife, //恢复生命
		EMP, //连锁攻击（迪妮莎-连锁EMP）
		MaxLife, //增加最大生命值
		addPart, //增加结算齿轮奖励百分比
		addScore, //增加结算分数奖励百分比
		addBaseReward, //增加结算转盘奖励次数
	}
	public enum BigTypeEnum {
		None, //没有
		Ban, //禁止
		Feel, //好感度
		Coin, //游戏币
		Exp, //经验
		Power, //当前血量
		MaxPower, //最大血量
		Part, //齿轮
		Key, //钥匙
		Character, //机师类
		Robot, //机体类
		Enemy, //怪物类
		TriggerSkill, //主动技能类
		PassiveSkill, //被动技能类
		FeelItem, //心之盒道具类型
		Item, //普通道具类型
		Level, //关卡类型
		FeelCount, //好感度奖励数量类型
		Photo, //海报类
		Music, //音乐类
		Ins, //Ins类
	}
	public enum AccountTypeEnum {
		Offline, //离线
		Online, //在线
		Error, //异常
	}
	public enum InstagramTypeEnum {
		AllGame, //所有游戏执行次数
		AllLevelGame, //通过所有关卡游戏次数
		TargetLevelGame, //通过指定关卡
		CharacterFeel, //机师好感度达到某值
		Rank, //排行榜名次
		CharacterShootDown, //机师击坠数
		ShowAdv, //观看广告
		MaxOnLine, //连续登陆天数
		MaxOnOff, //连续未登录天数
		UnLockCharacter, //解锁指定机师
		One, //选择1号奖励
		Two, //选择2号奖励
	}
	public enum GameTypeEnum {
		ALL, //所有游戏
		Level, //关卡游戏
		Ins, //剧情关卡游戏
		Box, //开宝箱(奖励)
		Training, //训练场游戏
		Difficult, //挑战关卡游戏
		Restore, //恢复生命
		Gas, //躲避毒气游戏
		Bird, //小鸟飞行游戏
		End, //关卡章节终点
	}
	public enum ErrorCodeEnum {
		SocketDisconnectError, //与服务器断开连接
		UserNameOrPassWordError, //用户名或密码错误
		AccountNotFoundError, //用户不存在
		NickNameExistError, //昵称已存在
		AccountIsFoundError, //用户已存在
		AccountCreateError, //用户创建失败
		AccountError, //帐号异常
		RepeatLoginError, //帐号已在登录状态
		AccountOfflineError, //目标用户是离线状态
		BalanceIsNotEnoughError, //余额不足
		DataBaseError, //数据库操作失败
		DataTableError, //数据表异常
		OutOfIndexError, //越界异常[和后台联系]
		CharacterDataError, //机师数据异常
		LackOfPartError, //齿轮数量不足
		PartToMysqlError, //游戏币更新数据库失败
		LackOfKeyError, //钥匙数量不足
		BuyError, //购买失败
		ReceiveError, //领取失败
		LackOfItemError, //道具数量不足
		IdentityError, //身份证号错误
		OnlineTimeOut, //在线超时
		HookError, //客户端有使用外挂嫌疑
	}
	public enum SingleUpdateTypeEnum {
		None, //无
		Feel, //机师好感度
		Part, //齿轮
		Key, //钥匙
		Music, //音乐
		Photo, //海报
		GameBird, //飞行小鸟游戏次数
		GameGas, //躲避毒气游戏次数
		GameTraining, //训练关卡胜利次数
		GameWin, //关卡游戏胜利次数
		GameLose, //所有游戏失败次数
		Ins, //ins剧情变更
		ShowAdv, //广告浏览次数
		GSMaxRank, //小游戏排名变更
		Level, //关卡步数变更
		Power, //当前生命值变更
		ShootDown, //击坠数变更
		Item, //道具变更（貌似只是好感度道具）
		InsRecord, //ins完成数变更
		MaxPower, //最大生命值变更
	}
	public class CommandCollection {
		public const string Sock = ".sock";
		public const string DB_ID_BASETABLE = "gamedb";
		public const bool EnableAutocommit = true;
		public const int FISH_ROOM_MAX = 1000;
		public const int FISH_ROOM_USER_MAX = 4;
		public const int SOCK_TYPE_LENGTH = 2;
		public const int SOCK_CONTEXT_LENGTH = 2;
		public const int SOCK_HEAD_LENGTH = 4;
		public const int UPLEVEL = 1000;
		public const int None = 0;
		public const int Ban = 1;
		public const int Feel = 6;
		public const int Coin = 22;
		public const int Exp = 33;
		public const int Power = 55;
		public const int MaxPower = 56;
		public const int Part = 66;
		public const int Key = 88;
		private static Dictionary<ProtoTypeEnum, System.Object> dataModel = new Dictionary<ProtoTypeEnum, object>();
		public static System.Object getDataModel(ProtoTypeEnum type, byte[] bytes){
			switch(type){
				case ProtoTypeEnum.CLogin:{
					CLogin.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CLogin.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CLogin.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CRegister:{
					CRegister.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CRegister.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CRegister.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SRegister:{
					SRegister.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SRegister.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SRegister.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CRegularization:{
					CRegularization.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CRegularization.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CRegularization.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SRegularization:{
					SRegularization.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SRegularization.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SRegularization.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SUserData:{
					SUserData.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SUserData.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SUserData.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CReNickName:{
					CReNickName.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CReNickName.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CReNickName.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SReNickName:{
					SReNickName.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SReNickName.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SReNickName.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CIdentity:{
					CIdentity.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CIdentity.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CIdentity.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SIdentity:{
					SIdentity.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SIdentity.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SIdentity.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CGetCharacterList:{
					CGetCharacterList.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CGetCharacterList.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CGetCharacterList.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SGetCharacterList:{
					SGetCharacterList.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SGetCharacterList.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SGetCharacterList.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CFeelReward:{
					CFeelReward.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CFeelReward.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CFeelReward.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SFeelReward:{
					SFeelReward.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SFeelReward.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SFeelReward.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CItems:{
					CItems.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CItems.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CItems.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SItems:{
					SItems.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SItems.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SItems.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CUseItem:{
					CUseItem.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CUseItem.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CUseItem.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CBaseReward:{
					CBaseReward.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CBaseReward.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CBaseReward.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SBaseReward:{
					SBaseReward.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SBaseReward.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SBaseReward.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CShowAdv:{
					CShowAdv.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CShowAdv.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CShowAdv.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SSingleUpdate:{
					SSingleUpdate.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SSingleUpdate.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SSingleUpdate.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CLevels:{
					CLevels.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CLevels.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CLevels.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SLevels:{
					SLevels.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SLevels.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SLevels.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CInses:{
					CInses.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CInses.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CInses.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SInses:{
					SInses.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SInses.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SInses.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CInsComplete:{
					CInsComplete.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CInsComplete.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CInsComplete.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CInsRecord:{
					CInsRecord.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CInsRecord.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CInsRecord.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SInsRecord:{
					SInsRecord.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SInsRecord.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SInsRecord.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CInsRecords:{
					CInsRecords.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CInsRecords.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CInsRecords.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SInsRecords:{
					SInsRecords.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SInsRecords.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SInsRecords.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CMusic:{
					CMusic.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CMusic.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CMusic.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SMusic:{
					SMusic.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SMusic.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SMusic.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CPhotos:{
					CPhotos.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CPhotos.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CPhotos.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SPhotos:{
					SPhotos.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SPhotos.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SPhotos.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CGetPhoto:{
					CGetPhoto.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CGetPhoto.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CGetPhoto.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CRank:{
					CRank.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CRank.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CRank.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SRank:{
					SRank.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SRank.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SRank.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.SError:{
					SError.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (SError.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = SError.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
				case ProtoTypeEnum.CHeart:{
					CHeart.Builder model;
					if(dataModel.ContainsKey(type)){
						model = (CHeart.Builder)dataModel[type];
						model.Clear();
						model.MergeFrom(bytes);
					} else {
						model = CHeart.ParseFrom(bytes).ToBuilder();
						dataModel.Add(type, model);
					}
					return model;
				}
			}
			return null;
		}
		public static System.Object getDataModel(ProtoTypeEnum type) {
			if (dataModel.ContainsKey(type)) {
				return dataModel[type];
			} else {
				switch(type){
					case ProtoTypeEnum.CLogin:{
						return CLogin.CreateBuilder();
				}
					case ProtoTypeEnum.CRegister:{
						return CRegister.CreateBuilder();
				}
					case ProtoTypeEnum.SRegister:{
						return SRegister.CreateBuilder();
				}
					case ProtoTypeEnum.CRegularization:{
						return CRegularization.CreateBuilder();
				}
					case ProtoTypeEnum.SRegularization:{
						return SRegularization.CreateBuilder();
				}
					case ProtoTypeEnum.SUserData:{
						return SUserData.CreateBuilder();
				}
					case ProtoTypeEnum.CReNickName:{
						return CReNickName.CreateBuilder();
				}
					case ProtoTypeEnum.SReNickName:{
						return SReNickName.CreateBuilder();
				}
					case ProtoTypeEnum.CIdentity:{
						return CIdentity.CreateBuilder();
				}
					case ProtoTypeEnum.SIdentity:{
						return SIdentity.CreateBuilder();
				}
					case ProtoTypeEnum.CGetCharacterList:{
						return CGetCharacterList.CreateBuilder();
				}
					case ProtoTypeEnum.SGetCharacterList:{
						return SGetCharacterList.CreateBuilder();
				}
					case ProtoTypeEnum.CFeelReward:{
						return CFeelReward.CreateBuilder();
				}
					case ProtoTypeEnum.SFeelReward:{
						return SFeelReward.CreateBuilder();
				}
					case ProtoTypeEnum.CItems:{
						return CItems.CreateBuilder();
				}
					case ProtoTypeEnum.SItems:{
						return SItems.CreateBuilder();
				}
					case ProtoTypeEnum.CUseItem:{
						return CUseItem.CreateBuilder();
				}
					case ProtoTypeEnum.CBaseReward:{
						return CBaseReward.CreateBuilder();
				}
					case ProtoTypeEnum.SBaseReward:{
						return SBaseReward.CreateBuilder();
				}
					case ProtoTypeEnum.CShowAdv:{
						return CShowAdv.CreateBuilder();
				}
					case ProtoTypeEnum.SSingleUpdate:{
						return SSingleUpdate.CreateBuilder();
				}
					case ProtoTypeEnum.CLevels:{
						return CLevels.CreateBuilder();
				}
					case ProtoTypeEnum.SLevels:{
						return SLevels.CreateBuilder();
				}
					case ProtoTypeEnum.CInses:{
						return CInses.CreateBuilder();
				}
					case ProtoTypeEnum.SInses:{
						return SInses.CreateBuilder();
				}
					case ProtoTypeEnum.CInsComplete:{
						return CInsComplete.CreateBuilder();
				}
					case ProtoTypeEnum.CInsRecord:{
						return CInsRecord.CreateBuilder();
				}
					case ProtoTypeEnum.SInsRecord:{
						return SInsRecord.CreateBuilder();
				}
					case ProtoTypeEnum.CInsRecords:{
						return CInsRecords.CreateBuilder();
				}
					case ProtoTypeEnum.SInsRecords:{
						return SInsRecords.CreateBuilder();
				}
					case ProtoTypeEnum.CMusic:{
						return CMusic.CreateBuilder();
				}
					case ProtoTypeEnum.SMusic:{
						return SMusic.CreateBuilder();
				}
					case ProtoTypeEnum.CPhotos:{
						return CPhotos.CreateBuilder();
				}
					case ProtoTypeEnum.SPhotos:{
						return SPhotos.CreateBuilder();
				}
					case ProtoTypeEnum.CGetPhoto:{
						return CGetPhoto.CreateBuilder();
				}
					case ProtoTypeEnum.CRank:{
						return CRank.CreateBuilder();
				}
					case ProtoTypeEnum.SRank:{
						return SRank.CreateBuilder();
				}
					case ProtoTypeEnum.SError:{
						return SError.CreateBuilder();
				}
					case ProtoTypeEnum.CHeart:{
						return CHeart.CreateBuilder();
				}
			}
			return null;
		}
	}
		public static byte[] getDataModelToByteArray(ProtoTypeEnum type, object model) {
			switch(type){
				case ProtoTypeEnum.CLogin:{
					return ((CLogin.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CRegister:{
					return ((CRegister.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SRegister:{
					return ((SRegister.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CRegularization:{
					return ((CRegularization.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SRegularization:{
					return ((SRegularization.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SUserData:{
					return ((SUserData.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CReNickName:{
					return ((CReNickName.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SReNickName:{
					return ((SReNickName.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CIdentity:{
					return ((CIdentity.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SIdentity:{
					return ((SIdentity.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CGetCharacterList:{
					return ((CGetCharacterList.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SGetCharacterList:{
					return ((SGetCharacterList.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CFeelReward:{
					return ((CFeelReward.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SFeelReward:{
					return ((SFeelReward.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CItems:{
					return ((CItems.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SItems:{
					return ((SItems.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CUseItem:{
					return ((CUseItem.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CBaseReward:{
					return ((CBaseReward.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SBaseReward:{
					return ((SBaseReward.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CShowAdv:{
					return ((CShowAdv.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SSingleUpdate:{
					return ((SSingleUpdate.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CLevels:{
					return ((CLevels.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SLevels:{
					return ((SLevels.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CInses:{
					return ((CInses.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SInses:{
					return ((SInses.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CInsComplete:{
					return ((CInsComplete.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CInsRecord:{
					return ((CInsRecord.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SInsRecord:{
					return ((SInsRecord.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CInsRecords:{
					return ((CInsRecords.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SInsRecords:{
					return ((SInsRecords.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CMusic:{
					return ((CMusic.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SMusic:{
					return ((SMusic.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CPhotos:{
					return ((CPhotos.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SPhotos:{
					return ((SPhotos.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CGetPhoto:{
					return ((CGetPhoto.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CRank:{
					return ((CRank.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SRank:{
					return ((SRank.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.SError:{
					return ((SError.Builder)model).Build().ToByteArray();
				}
				case ProtoTypeEnum.CHeart:{
					return ((CHeart.Builder)model).Build().ToByteArray();
				}
			}
			return null;
		}
		public static BigTypeEnum getBigTypeEnum(int itemId) {
			if (itemId >= 0 && itemId <= 0) {
				return BigTypeEnum.None;
			} else if (itemId >= 1 && itemId <= 1) {
				return BigTypeEnum.Ban;
			} else if (itemId >= 6 && itemId <= 6) {
				return BigTypeEnum.Feel;
			} else if (itemId >= 22 && itemId <= 22) {
				return BigTypeEnum.Coin;
			} else if (itemId >= 33 && itemId <= 33) {
				return BigTypeEnum.Exp;
			} else if (itemId >= 55 && itemId <= 55) {
				return BigTypeEnum.Power;
			} else if (itemId >= 56 && itemId <= 56) {
				return BigTypeEnum.MaxPower;
			} else if (itemId >= 66 && itemId <= 66) {
				return BigTypeEnum.Part;
			} else if (itemId >= 88 && itemId <= 88) {
				return BigTypeEnum.Key;
			} else if (itemId >= 1000 && itemId <= 1999) {
				return BigTypeEnum.Character;
			} else if (itemId >= 2000 && itemId <= 2999) {
				return BigTypeEnum.Robot;
			} else if (itemId >= 3000 && itemId <= 4999) {
				return BigTypeEnum.Enemy;
			} else if (itemId >= 5000 && itemId <= 5499) {
				return BigTypeEnum.TriggerSkill;
			} else if (itemId >= 5500 && itemId <= 5999) {
				return BigTypeEnum.PassiveSkill;
			} else if (itemId >= 6000 && itemId <= 6099) {
				return BigTypeEnum.FeelItem;
			} else if (itemId >= 6100 && itemId <= 6199) {
				return BigTypeEnum.Item;
			} else if (itemId >= 6201 && itemId <= 7000) {
				return BigTypeEnum.Level;
			} else if (itemId >= 7001 && itemId <= 7000) {
				return BigTypeEnum.FeelCount;
			} else if (itemId >= 10000 && itemId <= 10999) {
				return BigTypeEnum.Photo;
			} else if (itemId >= 11000 && itemId <= 11999) {
				return BigTypeEnum.Music;
			} else if (itemId >= 50000 && itemId <= 50999) {
				return BigTypeEnum.Ins;
			} else 
				return BigTypeEnum.None;
		}
	}
}